package data

type RaceMessage struct {
	MsgType string `json:"msgType"`
	Data RedundantRace `json:"data"`
}

type RedundantRace struct{
	Race Race `json:"race"`
}

type Race struct {
	Track Track `json:"track"`
	Cars []Car `json:"cars"`
	RaceSession RaceSession `json:"raceSession"`
}

type Track struct {
	Id string `json:"id"`
	Name string `json:"name"`
	Pieces []Piece `json:"pieces"`
	Lanes []Lane `json:"lanes"`
	StartingPoint StartingPoint `json:"startingPoint"`
}

type Piece struct {
	Length int64 `json:"length"`
	Angle int64 `json:"angle"`
	Radius int64 `json:"radius"`
	SwitchLane bool `json:"switch"`
}

type Lane struct {
	Index int64 `json:"index"`
	DistanceFromCenter int64 `json:"distanceFromCenter"`
}

type StartingPoint struct {
	Position Position `json:"position"`
	Angle float64 `json:"angle"`
}

type Position struct {
	X int64 `json:"x"`
	Y int64 `json:"y"`
}

type Car struct {
	Id CarId `json:"id"`
	Dimensions CarDimensions `json:"dimensions"`
}

type CarId struct {
	Name string `json:"name"`
	Color string `json:"color"`
}

type CarDimensions struct {
	Length float64 `json:"length"`
	Width float64 `json:"width"`
	GuideFlagPosition float64 `json:"guideFalgPosition"`
}

type RaceSession struct {
	Laps int `json:"laps"`
 	MaxLapTimeMs int `json:"maxLapTimeMs"`
	QuickRace bool `json:"quickRace"`
}

type CarPositions struct{
	Data []CarPosition `json:"data"`
}

type CarPosition struct {
	Id CarId `json:"id"`
	Angle float64 `json:"angle"`
	PiecePosition InPiecePosition `json:"piecePosition"`
}

type InPiecePosition struct{
	PieceIndex int `json:"pieceIndex"`
	InPieceDistance float64 `json:"inPieceDistance"`
	Lane PositionLane `json:"lane"`
	Lap int `json:"lap"`
}

type PositionLane struct{
	StartLaneIndex int8 `json:"startLaneIndex"`
	EndLaneIndex int8 `json:"endLaneIndex"`
}
