package main

import (
	"bufio"
	"errors"
	"fmt"
	"log"
	"os"
	"strconv"
	"com"
	"data"
	"encoding/json"
)

var(
	race data.RaceMessage
	carPositions map[string]data.CarPosition
)

func init() {
	carPositions = make(map[string]data.CarPosition)
}

func main() {

	host, port, name, key, err := parse_args()
	parse_error(err)

	fmt.Println("Connecting with parameters:")
	fmt.Printf("host=%v, port=%v, bot name=%v, key=%v\n", host, port, name, key)

	conn, err := com.Connect(host, port)
	parse_error(err)
	com.Initialize(bufio.NewReader(conn), bufio.NewWriter(conn))

	defer conn.Close()

	com.Send_join(name, key)
	err = bot_loop()
}

func bot_loop() (err error) {
	for {
		msgType, msgData, err := com.Read_msg()
		parse_error(err)
		dispatch_msg(msgType,msgData)
	}
	return
}

func dispatch_msg(msgType string, msgData []byte) (err error) {
	switch msgType {
	case "join":
		log.Printf("Joined")
		com.Send_ping()
	case "yourCar":
		log.Printf("Got car")
		com.Send_ping()
	case "gameInit":
		log.Printf("Game initialised")
		json.Unmarshal(msgData, &race)
		log.Printf(race.Data.Race.Track.Id)
		com.Send_ping()
	case "gameStart":
		log.Printf("Game started")
		com.Send_ping()
	case "crash":
		log.Printf("Someone crashed")
		com.Send_ping()
	case "gameEnd":
		log.Printf("Game ended")
		com.Send_ping()
	case "carPositions":
		var carPositions data.CarPositions
		json.Unmarshal(msgData, &carPositions)
		updateCarPostions(carPositions)

		req_switch, direction := switch_required()
		if req_switch {
			com.Send_switch(direction)
		} else {
			com.Send_throttle(0.65)
		}
	case "error":
		log.Printf(fmt.Sprintf("Got error: %v", msgData))
		com.Send_ping()
	case "tournamentEnd":
		log.Printf("Tournament Ended")
		os.Exit(0)
	default:
		log.Printf("Got msg type: %s", msgType)
		com.Send_ping()
	}
	return
}

func parse_args() (host string, port int, name string, key string, err error) {
	args := os.Args[1:]
	if len(args) != 4 {
		return "", 0, "", "", errors.New("Usage: ./run host port botname botkey")
	}
	host = args[0]
	port, err = strconv.Atoi(args[1])
	if err != nil {
		return "", 0, "", "", errors.New(fmt.Sprintf("Could not parse port value to integer: %v\n", args[1]))
	}
	name = args[2]
	key = args[3]

	return
}

func updateCarPostions(pos data.CarPositions) {
	for _,cp := range pos.Data {
		carPositions[cp.Id.Name] = cp
	}
}

func parse_error(err error) {
	if err != nil {
		log_and_exit(err)
	}
}

func log_and_exit(err error) {
	log.Fatal(err)
	os.Exit(1)
}

func switch_required() (req bool, direction string) {
	req = false
	direction = "left"
	return
}
