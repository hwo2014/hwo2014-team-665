package com

import (
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net"
	"os"
)

var(
	reader *bufio.Reader
	writer *bufio.Writer
)

func Initialize(r *bufio.Reader, w *bufio.Writer) {
	reader = r
	writer = w
}

func Connect(host string, port int) (conn net.Conn, err error) {
	conn, err = net.Dial("tcp", fmt.Sprintf("%s:%d", host, port))
	return
}

func Send_join(name string, key string) (err error) {
	data := make(map[string]string)
	data["name"] = name
	data["key"] = key
	err = write_msg("join", data)
	return
}

func Send_ping() (err error) {
	err = write_msg("ping", make(map[string]string))
	return
}

func Send_throttle(throttle float32) (err error) {
	err = write_msg("throttle", throttle)
	return
}

func Send_switch(direction string) (err error) {
	err = write_msg("switchLane", direction)
	return
}

func Parse_input(input interface{}) (msgType string, data interface{}, err error) {
	switch t := input.(type) {
	default:
		err = errors.New(fmt.Sprintf("Invalid message type: %T", t))
		return
	case map[string]interface{}:
		var msg map[string]interface{}
		var ok bool
		msg, ok = input.(map[string]interface{})
		if !ok {
			err = errors.New(fmt.Sprintf("Invalid message type: %v", msg))
			return
		}
		msgType = msg["msgType"].(string)
		switch msg["data"].(type) {
			case interface{}:
				data = msg["data"].(interface{})
				return
			default:
				return
		}
	}
	return
}

func get_msgType(input interface{}) (msgType string, err error) {
	switch t := input.(type) {
	default:
		err = errors.New(fmt.Sprintf("Invalid message type: %T", t))
		return
	case map[string]interface{}:
		var msg map[string]interface{}
		var ok bool
		msg, ok = input.(map[string]interface{})
		if !ok {
			err = errors.New(fmt.Sprintf("Invalid message type: %v", msg))
			return
		}
		msgType = msg["msgType"].(string)
		return
	}
	return
}

func write_msg(msgtype string, data interface{}) (err error) {
	m := make(map[string]interface{})
	m["msgType"] = msgtype
	m["data"] = data
	var payload []byte
	payload, err = json.Marshal(m)
	_, err = writer.Write([]byte(payload))
	if err != nil {
		return
	}
	_, err = writer.WriteString("\n")
	if err != nil {
		return
	}
	writer.Flush()
	return
}

func Read_msg() (msgType string, msgData []byte, err error) {
	var line string
	var parsedMmsg interface{}
	line, err = reader.ReadString('\n')
	if err != nil {
		return
	}
	err = json.Unmarshal([]byte(line), &parsedMmsg)
	parse_error(err)
	msgType, err = get_msgType(parsedMmsg)
	parse_error(err)
	msgData = []byte(line)
	return
}

func parse_error(err error) {
	if err != nil {
		log_and_exit(err)
	}
}

func log_and_exit(err error) {
	log.Fatal(err)
	os.Exit(1)
}
